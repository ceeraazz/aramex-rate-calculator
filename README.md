ARAMEX RATE CALCULATOR AND TRACKING CODE PROVIDER.

Using this package, you can set your location and the charges to those locations. Then by using the member functions of the respective classs, the total charges with respect to the weight can be retrieved.

Installation:
Require the siraj/ratetrack package in your composer.json and update your dependencies:

$ composer require siraj/ratetrack

Then, add Ratetrack\RateServiceProvider, to your config/app.php providers array:

Ratetrack\RateServiceProvider::class,

DONE!! ENJOY!!
