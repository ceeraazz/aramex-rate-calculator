<?php
namespace Ratetrack;

use Illuminate\Support\ServiceProvider;

class RateServiceProvider extends  ServiceProvider {
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/./../resources/views/', 'ratetrack');
    }

    public function register()
    {
        $this->registerPublishables();
    }

    private function registerPublishables()
    {
        $basePath = dirname(__DIR__);
        $arrPublishables = [
            'migrations' => [
                "$basePath/publishable/database/migrations" => database_path('migrations'),
            ],
            'config' => [
                "$basePath/publishable/config/ratetrack.php" => config_path('ratetrack.php'),
            ]
        ];

        foreach ($arrPublishables as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }
}