<?php

namespace Ratetrack\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ratetrack\Models\ShippingRates;
use Ratetrack\Models\Trackcode;
use Ratetrack\Models\Trackingcode;

class RateController extends Controller
{
    public function index()
    {
        return view('ratetrack::rate-index');
    }

    public function test()
    {
        return "test route";
    }
    public function store_code()
    {
        Trackingcode::create([
            'code' => $_REQUEST['code'],
            'is_used' => 0,
        ]);
        return redirect()->back();
    }

    public function getTrackingCode()
    {
        return Trackcode::getTrackingCode();
    }

    public function makeCodeUsed(Request $code)
    {
        return Trackcode::setTrackingCodeStatus($code);
    }
    public function setrate(Request $request) {
        ShippingRates::setrate($request->city, $request->rate_1kg, $request->rate_additional_kg, $request->info);
        return back();
    }
    public function getrate() {
        $result = ShippingRates::getRate("dhangadhi",2.5);
        dd($result);
    }

}