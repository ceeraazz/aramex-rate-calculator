<?php

namespace Ratetrack\Models;

use Illuminate\Http\Request;

class Trackcode
{
    public static function getTrackingCode()
    {
        $code = Trackingcode::where('is_used', 0)->first();
        if($code == null){
            echo("No free transaction codes found!");
            return null;
        }else {
            return $code->code;
        }

    }

    public static function setTrackingCodeStatus(Request $request)
    {
        Trackingcode::where('code', $request['code'])->first()->update(['is_used' => 1]);
        return back();
    }
}