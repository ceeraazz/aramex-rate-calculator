<?php
/**
 * Created by PhpStorm.
 * User: siraj
 * Date: 3/19/2019
 * Time: 12:58 PM
 */

namespace Ratetrack\Models;


class ShippingRates
{
    protected $city;
    protected $weight;
    public static function setrate($city,$rate_1kg,$rate_additional_kgs, $info = null )
    {
            ShippingRate::create([
                'city' => $city,
                'rate_1kg' => $rate_1kg,
                'rate_additional_kg' => $rate_additional_kgs,
                'info' => $info
            ]);
            return true;

    }
    public static function getRate($city, float $weight)
    {
        $data = ShippingRate::where('city', $city)->first();
        $rate = $data->rate_1kg + (ceil($weight) - 1) * $data->rate_additional_kg;
        return collect(['rate' => $rate, 'description' => $data->description]);
    }
}