<?php
$namespace = 'Ratetrack\Http\Controllers';

Route::group([
    'namespace' => $namespace,
    'prefix' => 'rate',
    'middleware' => 'web'
], function () {
    Route::get('/', 'RateController@index');
    Route::get('/test', 'RateController@test');
    Route::post('/store-code', 'RateController@store_code')->name('store-code');
    Route::get('/trackingcode', 'RateController@getTrackingCode')->name('gettrackingcode');
    Route::get('/makeused/{code}', 'RateController@makeCodeUsed');
    Route::get('/setrate', 'RateController@setrate');
    Route::get('/getrate', 'RateController@getrate');
});

Route::get('demo', function () {
    return "this is a demo route!";
});